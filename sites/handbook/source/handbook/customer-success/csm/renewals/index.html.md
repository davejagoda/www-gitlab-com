title: "Customer Renewal Tracking"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM-related handbook pages.

Link to [Commercial Renewal Process](/handbook/customer-success/comm-sales/renewals/)

## Overview

A key part of the customer relationship lifecycle is the renewal phase. CSMs must proactively track the renewal dates of their customers and align with their Account Executive (AE) to ensure that a customer renewal takes place.

## Timeframe

4 months before the renewal date for a customer, a CTA will trigger in Gainsight to prompt the CSM to reach out to their aligned account team to discuss strategy and next steps for the renewal. The expectation is that the team will meet internally and speak with the customer regarding the renewal within the following 30 days. This leaves three months to execute any renewal strategy, such as an account triage.

## Renewal playbook steps

1. Renewal Review: Internal sync with SAE/AE and potentially SA as defined below
1. CSM asks 'Soft' Renewal Question
1. Update `CSM Sentiment` as appropriate

Once the above tasks are complete, the playbook and CTA can be closed out as all actionable items have been performed.

## Renewal review meeting - internal sync

A renewal review meeting should have the following attendees:

- Account Executive
- Customer Success Manager
- Solutions Architecht, if an upsell is being discussed

For CSMs with larger books of business, it is appropriate to update an AE or SAE async that they are going to ask the soft renewal question in their next sync, to ensure they are aligned with other conversations that are ensuing.  

## `Soft` renewal question

The CSM will ask the initial renewal question, this should be a ['soft'](https://www.mbaskool.com/business-concepts/marketing-and-strategy-terms/7214-soft-fact-questions.html) question to see if there is any risk in the account and to provide time to mitigate any risk.  An example of this soft question is, "I know you have your renewal coming up in the next few months - how are you feeling about the renewal currently, and are there any changes you are considering that we can prepare for?"

If the status of the renewal is already known (renewal conversation with SAE or AE has happened, negotiations or contract is in-flight), the CSM can close the renewal CTA selecting the appropriate close reason. 

## Renewal plan

The action items created from the “Renewal Review” meeting should be incorporated into the CSM customer cadence meetings and into any pending QBRs. The actual renewal plan will be documented by the SAE or AE in Salesforce.

## Tracking renewal risk

Please see [Customer Health Assessment and Management](/handbook/customer-success/csm/health-score-triage/)

## Related Processes

[Customer Success Escalations Process](/handbook/customer-success/csm/escalations/)